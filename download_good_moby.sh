#!/usr/bin/env bash

src="https://www.star.nesdis.noaa.gov/socd/moby/filtered_spec/"
GREEN="007100" # green color on webpage is good data 

usage()
{
	echo download_good_moby.sh [-d date ] [-h] [-l]
	echo -d date   with date is specified as YYYY, YYYYMM, or YYYYMMDD
	echo -l        list files only
	echo -h        print help
}
while getopts ":hld:" option; do
   case $option in
      h) usage; exit;;
      d) date=$OPTARG;;
	  l) dolist=1;;
     \?) echo "download_good_moby: bad option -- $option" ; usage; exit;;
   esac
done
if [ $OPTIND -eq 1 ]; then usage; exit; fi

filepat=$date\\d*d.txt
files=$(curl -s $src | grep $GREEN  | grep -E "$filepat"| cut -f2 -d\")
array=($files)
if [ $dolist -eq 1 ]; then
	for moby_file in ${array[*]}; do echo $moby_file; done
	echo ${#array[@]} files
else
	for moby_file in ${array[*]}; do 
		echo downloading $src/$moby_file
		curl -s --output $moby_file $src/$moby_file 
    done
fi
